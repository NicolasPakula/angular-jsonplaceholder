import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PostsComponent } from './posts/posts.component';
import { UserListComponent } from './user-list/user-list.component';

const routes: Routes = [
  // redirection
  {path: "", redirectTo: "users", pathMatch: "full"},
  // Routes pour la section utilisateur
  {path: "users", component: UserListComponent, pathMatch: "full"},
  {path: "users/:id", component: UserListComponent, pathMatch: "full"},
  // Routes pour la section articles
  {path: "posts", component: PostsComponent, pathMatch: "full"},
  {path: "posts/:id", component: PostsComponent, pathMatch: "full"},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
