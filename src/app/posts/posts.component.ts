import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Comment } from '../shared/models/comment';
import { Post } from '../shared/models/post';
import { CommentService } from '../shared/services/comment.service';
import { PostService } from '../shared/services/post.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  posts: Post[] = [];
  reduce: boolean = true;
  comments: Comment[] = [];
  constructor(private postService: PostService, private route: ActivatedRoute,private commentService: CommentService) { }

  //Si on appelle le componnent avec un param id, affichera un seul post mais avec ses commentaires
  ngOnInit(): void {
    //un post et ses commentaires
    if(Number(this.route.snapshot.paramMap.get('id')!=null)){
      this.reduce = false;
      this.postService.getUsersById(Number(this.route.snapshot.paramMap.get('id'))).subscribe((posts:Post)=>{
        this.posts.push(posts);
      })
      this.commentService.getComments(Number(this.route.snapshot.paramMap.get('id'))).subscribe((comments:Comment[])=>{
        this.comments = comments;
      })
    }
    //liste des posts
    else{
      this.reduce = true;
      this.postService.getUsers().subscribe((posts:Post[])=>{
        this.posts = posts;
      })
    }
  }

}
