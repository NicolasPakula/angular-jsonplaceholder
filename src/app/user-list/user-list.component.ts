import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Comment } from '../shared/models/comment';
import { User } from '../shared/models/user';
import { CommentService } from '../shared/services/comment.service';
import { UserService } from '../shared/services/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
})
export class UserListComponent implements OnInit {
  users: User[] = [];
  comments: Comment[]= [];

  constructor(private userService: UserService, private route: ActivatedRoute,private commentService:CommentService) {}
  //un param id pour le component = un user, sinon sans param affiche la liste complète
  ngOnInit(): void {
    if(Number(this.route.snapshot.paramMap.get('id')!=null)){
      this.userService.getUsersById(Number(this.route.snapshot.paramMap.get('id'))).subscribe((users:User)=>{
        this.users.push(users);
      })
   }
   else{
    this.userService.getUsers().subscribe((users:User[])=>{
      this.users = users;
    })
   }
  }
}
